﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ShopBridgeInventory.Models
{
    public class InventoryRepository : ShopBridgeInventory.Models.IInventoryRepository
    {
        private ShopBridgeEntities db = new ShopBridgeEntities();

        public IQueryable<tbl_inventory> ListInventorys()
        {
            return db.tbl_inventory;
        }

        public Task<tbl_inventory> GetInventory(int id)
        {
            return db.tbl_inventory.FindAsync(id);
        }

        public bool CreateInventory(tbl_inventory inventoryToCreate)
        {
            try
            {
                db.tbl_inventory.Add(inventoryToCreate);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public Task<tbl_inventory> UpdateInventory(int id, tbl_inventory inventory)
        {
            try
            {
                db.Entry(inventory).State = EntityState.Modified;
                db.SaveChanges();
                return db.tbl_inventory.FindAsync(id);
            }
            catch
            {
                return db.tbl_inventory.FindAsync(id);
            }
        }

    }

    public interface IInventoryRepository
    {
        bool CreateInventory(tbl_inventory inventoryToCreate);
        Task<tbl_inventory> UpdateInventory(int id, tbl_inventory inventoryToCreate);
        IQueryable<tbl_inventory> ListInventorys();
        Task<tbl_inventory> GetInventory(int id);
    }
}
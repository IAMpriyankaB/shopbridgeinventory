﻿using ShopBridgeInventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ModelBinding;

namespace ShopBridgeInventory.Services
{
    public class InventoryService: IInventoryService
    {
        private ModelStateDictionary _modelState;
        private IInventoryRepository _repository;

        public InventoryService(ModelStateDictionary modelState, IInventoryRepository repository)
        {
            _modelState = modelState;
            _repository = repository;
        }

        protected bool ValidateInventory(tbl_inventory inventoryToValidate)
        {
            if (inventoryToValidate.Name.Trim().Length == 0)
                _modelState.AddModelError("Name", "Name is required.");
            if (inventoryToValidate.Description.Trim().Length == 0)
                _modelState.AddModelError("Description", "Description is required.");
            if (inventoryToValidate.Price< 1)
                _modelState.AddModelError("Price", "Price cannot be zero.");
            if (inventoryToValidate.units < 0)
                _modelState.AddModelError("units", "Units in stock cannot be less than zero.");
            return _modelState.IsValid;
        }

        public IQueryable<tbl_inventory> ListInventorys()
        {
            return _repository.ListInventorys();
        }

        public Task<tbl_inventory> GetInventory(int id)
        {
            return _repository.GetInventory(id);
        }

        public bool CreateInventory(tbl_inventory InventoryToCreate)
        {
            // Validation logic
            if (!ValidateInventory(InventoryToCreate))
                return false;

            // Database logic
            try
            {
                _repository.CreateInventory(InventoryToCreate);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public Task<tbl_inventory> UpdateInventory(int id, tbl_inventory InventoryToUpdate)
        {
            // Validation logic
            if (!ValidateInventory(InventoryToUpdate))
                return _repository.GetInventory(0);

            // Database logic
            try
            {
                _repository.UpdateInventory(id, InventoryToUpdate);
            }
            catch
            {
                return _repository.GetInventory(0);
            }
            return _repository.GetInventory(0);
        }
    }

    public interface IInventoryService
    {
        bool CreateInventory(tbl_inventory InventoryToCreate);
        Task<tbl_inventory> UpdateInventory(int id, tbl_inventory inventoryToCreate);
        IQueryable<tbl_inventory> ListInventorys();
        Task<tbl_inventory> GetInventory(int id);
    }
}
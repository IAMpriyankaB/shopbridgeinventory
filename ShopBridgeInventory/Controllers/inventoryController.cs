﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ShopBridgeInventory.Models;
using ShopBridgeInventory.Services;

namespace ShopBridgeInventory.Controllers
{
    public class inventoryController : ApiController
    {
        private ShopBridgeEntities db = new ShopBridgeEntities();

        private IInventoryService _repository;

        public inventoryController()
        {
            _repository = new InventoryService(this.ModelState, new InventoryRepository());
        }


        public inventoryController(IInventoryService repository)
        {
            _repository = repository;
        }

        // GET: api/inventory
        public IQueryable<tbl_inventory> Getinventory()
        {
            return _repository.ListInventorys();
        }

        // GET: api/inventory/5
        [ResponseType(typeof(tbl_inventory))]
        public async Task<IHttpActionResult> Getinventory(int id)
        {
            tbl_inventory inventory = await _repository.GetInventory(id);
            if (inventory == null)
            {
                return NotFound();
            }

            return Ok(inventory);
        }

        // PUT: api/inventory/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putinventory(int id, tbl_inventory inventory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventory.inven_ID)
            {
                return BadRequest();
            }
            

            try
            {
                await _repository.UpdateInventory(id,inventory);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!inventoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/inventory
        [ResponseType(typeof(tbl_inventory))]
        public async Task<IHttpActionResult> Postinventory(tbl_inventory inventory)
        {
            if (!_repository.CreateInventory(inventory))
                return CreatedAtRoute("DefaultApi", new { id = inventory.inven_ID }, inventory);
            return BadRequest();
        }

        // DELETE: api/inventory/5
        [ResponseType(typeof(tbl_inventory))]
        public async Task<IHttpActionResult> Deleteinventory(int id)
        {
            tbl_inventory inventory = await _repository.GetInventory(id);
            if (inventory == null)
            {
                return NotFound();
            }

            inventory.markdeleted = true;
            
            await _repository.UpdateInventory(id, inventory);

            return Ok(inventory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool inventoryExists(int id)
        {
            return db.tbl_inventory.Count(e => e.inven_ID == id) > 0;
        }
    }
}